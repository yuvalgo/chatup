import { Component, OnInit , OnDestroy} from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import {Router} from "@angular/router";
import {UserService} from "../shared/user.service";

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit, OnDestroy {
  user;

  rooms;

  roomsArr;

  newRoom = "";

  constructor(private db: AngularFireDatabase, private router: Router, private userService: UserService) {
    this.rooms = db.list('/rooms');
 //   this.router.navigate(['/chat', 'default']);
  }
flag = true;
  ngOnDestroy(){
    // this.rooms.unsubscribe();
  }

  ngOnInit() {
    this.user = this.userService.getUser();
    this.rooms
      .subscribe(snapshots => {
        this.roomsArr = [];
        if(this.flag)
        snapshots.forEach(snapshot => {
          console.log(snapshot.users)
          if(snapshot.users){
          if(!snapshot.users.forEach){
            snapshot.users = this.objToArrys(snapshot.users);
          }
          snapshot.users.forEach(user => {
            if (user.email === this.user.email) {
              snapshot.show = true;
            }
          });
          }
          this.roomsArr.push(snapshot);
        });
      });
  }

  objToArrys(obj){
    let res = [];
    for(let key in obj){
      res[key] = obj[key];
    }
    return res;
  }

  createRoom(){
     location.reload();
    this.flag = false;
    this.rooms.update(this.newRoom, {
      name: this.newRoom,
      users: [{
        uid: this.user.uid,
        displayName: this.user.displayName,
        email: this.user.email,
      }]
    });
     this.selectRoom(this.newRoom);
    this.newRoom = "";
  }

  selectRoom(room) {
    this.router.navigate(['/chat', room.name]);
  //  location.reload();
  }

}
