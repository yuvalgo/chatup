import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { LoginComponent } from './login/login.component';
import {rootRouterConfig} from "./app.routes";
import {RouterModule} from "@angular/router";
import {AuthGuard} from "./auth.service";
import { RoomsComponent } from './rooms/rooms.component';
import {UserService} from "./shared/user.service";
import { ChatComponent } from './chat/chat.component';

import {SignupComponent} from "./signup/signup.component";
import { EmailComponent } from "./email/email.component";

// Must export the config
export const firebaseConfig = {
    apiKey: "AIzaSyBNgr2a1uLa7uQeMk_IeEvFRc7scWR4ytc",
    authDomain: "chatup-33e9e.firebaseapp.com",
    databaseURL: "https://chatup-33e9e.firebaseio.com",
    projectId: "chatup-33e9e",
    storageBucket: "chatup-33e9e.appspot.com",
    messagingSenderId: "795021536344"
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RoomsComponent,
    ChatComponent,
    SignupComponent,
    EmailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  providers: [UserService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
