import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import {AngularFireDatabase} from "angularfire2/database";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {

  state: string = '';
  error: any;

  firstName = "";

  lastName = "";

  users;

  rooms;

  constructor(public afAuth: AngularFireAuth,private router: Router, private db: AngularFireDatabase) {
    this.users = db.list('/users');
    this.rooms = db.list('/rooms');
  }

  onSubmit(formData) {
    if(formData.valid) {
      console.log(formData.value);
      let email= formData.value.email;
      let password =  formData.value.password;
      this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(
        (success) => {
          console.log(success)

          this.users.push({
            uid: success.uid,
            displayName: this.firstName  + " " + this.lastName,
            email: success.email,
          });

          let room;
          this.rooms.subscribe(snapshots => {
            snapshots.forEach(snapshot => {
              if (snapshot.name === "default") {
                room = snapshot;
              }
            });
          });

          setTimeout(() => {
            if (!room.users) {
              room.users = [];
            }
            room.users.push({
              uid: success.uid,
              displayName: this.firstName  + " " + this.lastName,
              email: success.email,
            });
            this.rooms.update('default', room);
          }, 5000);

    const users = this.db.list('/rooms/default/users')
      .update((Math.floor((Math.random() * 10000000) + 1)).toString(), {
             uid: success.uid,
            displayName: this.firstName  + " " + this.lastName,
            email: success.email,
          }     
      
      );

          this.router.navigate(['/login-email']);
        }).catch(
        (err) => {
          this.error = err;
        })
    }
  }

  ngOnInit() {
  }

}
