import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../shared/user.service';
import {AngularFireDatabase} from 'angularfire2/database';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  user;

  rooms;

  messages;

  mess = '';

  users;

  id = '';

  userInRom;

  usersData;

  constructor(public route: ActivatedRoute, private db: AngularFireDatabase, private router: Router, private userService: UserService) {
    this.rooms = db.list('/rooms');
    this.users = db.list('/users');

  }

  ngOnInit() {
    this.route
      .params
      .subscribe(params => {
        console.log(params['id']);
        this.user = this.userService.getUser();

        this.users.subscribe(users => this.usersData = users);

        this.rooms
          .subscribe(snapshots => {
            let auth = false;
            this.id = params['id'];
            snapshots.forEach(snapshot => {
              if (snapshot.users && snapshot.name === params['id']) {
                if (!snapshot.users.forEach){
                  snapshot.users = this.objToArrys(snapshot.users);
                  this.userInRom = this.objToArrysPush(snapshot.users);
                } else {
                  this.userInRom = snapshot.users;
                }

                snapshot.users.forEach((user, index) => {

                  if (user.email === this.user.email) {
                    auth = true;
                    this.messages = this.db.list('/rooms/' + params['id'] + '/messages');
                  }
                });
              };
            });
            if (!auth)
              this.router.navigate([ '/rooms' ]);

          });
      });
  }

  sendMessage(){
    console.log(this.user.displayName, this.user)
    this.messages.push({text: this.mess, author: this.user.displayName , time: new Date().toString()});

    this.mess = '';
  }

  nameInvite = '';

  inviteuser = {displayName: ""};

  inviteTo(){
    console.log(this.inviteuser)
    const users = this.db.list('/rooms/' + this.id + '/users')
      .update((Math.floor((Math.random() * 1000000) + 1)).toString(), this.inviteuser);

    this.nameInvite = '';
  }

  objToArrys(obj){
    const res = [];
    for (const key in obj){
      res[key] = obj[key];
    }
    return res;
  }
  objToArrysPush(obj){
    const res = [];
    for (const key in obj){
      res.push(obj[key]);
    }
    return res;
  }
}
