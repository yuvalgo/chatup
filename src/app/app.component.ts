
import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {UserService} from "./shared/user.service";
import {User2} from "./shared/user.model";

import {LoginComponent} from "./login/login.component";
import { AfterViewInit, ViewChild } from '@angular/core';
import {Router} from "@angular/router";
import { Cookie } from 'ng2-cookies';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // directives: [LoginComponent],
  styleUrls: ['./app.component.css'],


})
export class AppComponent  implements AfterViewInit {
  user: Observable<firebase.User>;
   @ViewChild(LoginComponent)
  private userControl: LoginComponent; 

  ngAfterViewInit() {
    // Redefine `seconds()` to get from the `CountdownTimerComponent.seconds` ...
    // but wait a tick first to avoid one-time devMode
    // unidirectional-data-flow-violation error
  }

  constructor(public afAuth: AngularFireAuth, private userService: UserService, private router: Router) {
    this.user = afAuth.authState;
  }

logout2() {     console.log("asdsadsada");
    this.afAuth.auth.signOut();
    Cookie.set("User", null);
    this.router.navigate([ '/login-email' ]); }
}

    
